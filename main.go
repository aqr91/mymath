// Написать потокобезопасный кеш с алгоритмом вытеснения LFU

package main

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

const (
	maxSize int = 2
)

type LFUCache struct {
	mu           sync.RWMutex
	counterMap   map[string]int
	cacheWrapper *cacheWrapper
}

func NewLFUCache() *LFUCache {
	return &LFUCache{
		counterMap:   make(map[string]int),
		cacheWrapper: newCacheWrapper(),
	}
}

func (c *LFUCache) Set(key string, value interface{}) (interface{}, error) {
	if len(c.cacheWrapper.cache.cache) >= maxSize {
		var keyToDelete string
		var counterToDelete = 100
		c.mu.Lock()
		for keyFromMap, counterFromMap := range c.counterMap {
			if counterFromMap < counterToDelete {
				counterToDelete = counterFromMap
				keyToDelete = keyFromMap
			}
		}
		delete(c.counterMap, keyToDelete)
		delete(c.cacheWrapper.cache.cache, keyToDelete)
		c.mu.Unlock()
	}
	val, err := c.cacheWrapper.Set(key, value)
	if err != nil {
		return val, err
	} else {
		c.mu.Lock()
		c.counterMap[key] = 0
		c.mu.Unlock()
		return nil, nil
	}
}

func (c *LFUCache) Get(key string) (interface{}, error) {
	val, err := c.cacheWrapper.Get(key)
	if err != nil {
		return nil, err
	} else {
		c.mu.Lock()
		c.counterMap[key]++
		c.mu.Unlock()
		return val, nil
	}
}

type cacheWrapper struct {
	mu    sync.RWMutex
	cache *cache
}

func newCacheWrapper() *cacheWrapper {
	return &cacheWrapper{
		cache: &cache{
			cache: make(map[string]interface{}),
		},
	}
}

type valueError struct {
	value interface{}
	err   error
}

func (c *cacheWrapper) Set(key string, value interface{}) (interface{}, error) {
	ch := make(chan valueError)
	go func() {
		val, err := c.cache.Set(key, value)
		valErr := valueError{
			value: val,
			err:   err,
		}
		ch <- valErr
		close(ch)
	}()
	select {
	case kv := <-ch:
		return kv.value, kv.err
	case <-time.After(10 * time.Second):
		return nil, errors.New("timer deadline exceed")
	}
}

func (c *cacheWrapper) Get(key string) (interface{}, error) {
	ch := make(chan valueError)
	go func() {
		val, err := c.cache.Get(key)
		valErr := valueError{
			value: val,
			err:   err,
		}
		ch <- valErr
		close(ch)
	}()
	select {
	case kv := <-ch:
		return kv.value, kv.err
	case <-time.After(10 * time.Second):
		return nil, errors.New("timer deadline exceed")
	}
}

type cache struct {
	cache map[string]interface{}
	mu    sync.RWMutex
}

// Set реализован по принципу есть значение - получил старое и error, перезаписал ключ
// новым значением. Нет ключа - получил nil, nil, записал в мапу ключ - значение.
func (c *cache) Set(key string, value interface{}) (interface{}, error) {
	c.mu.RLock()
	if oldValue, ok := c.cache[key]; ok {
		c.mu.RUnlock()
		c.mu.Lock()
		c.cache[key] = value // пишем
		c.mu.Unlock()
		return oldValue, errors.New("ключ уже существует")
	} else {
		c.mu.RUnlock() // нужно для того что бы мы гарантированно разлочили mutex
	}
	c.mu.Lock()
	c.cache[key] = value
	c.mu.Unlock()
	return nil, nil
}

// Get реализован по принципу нет ключа - получил nil и ошибку, есть ключ -
// получил значение и nil
func (c *cache) Get(key string) (interface{}, error) {
	c.mu.RLock()
	if val, ok := c.cache[key]; !ok {
		c.mu.RUnlock()
		return nil, errors.New("ключа не найдено")
	} else {
		c.mu.RUnlock()
		return val, nil
	}
}

func main() {
	lfu := NewLFUCache()
	lfu.Set("1", "key1")
	lfu.Set("2", "key123")
	lfu.Get("2")
	lfu.Get("2")
	lfu.Get("1")
	lfu.Set("3", "key3")
	lfu.Set("4", "key4")

	time.Sleep(1 * time.Second)
	fmt.Println("мапа")
	for k, v := range lfu.cacheWrapper.cache.cache {
		fmt.Printf("key %v, value %v\n", k, v)
	}

	time.Sleep(1 * time.Second)
	fmt.Println("счетчик")
	for k, v := range lfu.counterMap {
		fmt.Printf("key %v, value %v\n", k, v)
	}
}
